import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

print("Задача 1. Нахождение параметров модели аналитически:")
df = pd.read_csv("weights_heights.csv", index_col=0)
df.plot.scatter( x="Weight", y="Height")
X_train = df["Weight"].as_matrix()
X_train.shape
y_train = df["Height"].as_matrix()
y_train.shape
X = np.array([np.ones(X_train.shape[0]), X_train]).T
X.shape
a = np.dot(X.T,X)
b = np.dot(X.T, y_train)
a.shape
b.shape
w = np.linalg.solve(a,b)

print("%.3f" % w[0])
print("%.3f" % w[1])

y_hat = np.dot(w, X.T) 

def mse(y, y_hat):
    return (1/len(y)) * np.sum(np.square(y_hat-y))

mse(y_train, y_hat)
x_min = df["Weight"].min()
x_max = df["Weight"].max()
df.plot.scatter(x="Weight", y="Height")
plt.plot([x_min,x_max], [x_min*w[1]+w[0],x_max*w[1]+w[0]], color="red")
# plt.show()

print("Задача 2. Нахождение параметров модели минимизацией функционала ошибки градиентным спуском:")
X = X_train.reshape(-1,1)
y = y_train.reshape(-1,1)

def center(val):
    val = (val - X.mean()) / X.std()
    val = np.c_[np.ones(val.shape[0]), val] 
    return val

def mse(y, y_hat):
    return (1/len(y)) * np.sum(np.square(y_hat-y))

def cal_cost(w,X,y):
    predictions = X.dot(w)
    cost = mse(y, predictions)
    return cost

def grad_desent(X, y, w_init=[70.1, 0.8], eta=1e-4, max_iter=1e4, min_weight_dist=1e-8):
    X = center(X)
    w = np.array(w_init).reshape(-1,1)
    learning_rate = eta
    iterations = int(max_iter)    
    m = len(y)
    cost_history = np.zeros(iterations)
    for it in range(iterations):        
        prediction = np.dot(X,w)        
        diff = (1/m)*learning_rate*( X.T.dot((prediction - y)))
        if (abs((diff)) < min_weight_dist).any():
            cost_history = cost_history[:it]
            break
        w = w - diff
        cost_history[it]  = cal_cost(w,X,y)        
    return w, cost_history
w,cost_history = grad_desent(X, y, w_init=[70.1, 0.8], eta=1e-3, max_iter=1e4, min_weight_dist=1e-8)
fig,ax = plt.subplots(figsize=(12,8))
ax.set_ylabel('J(w)')
ax.set_xlabel('Iterations')
_=ax.plot(range(len(cost_history)),cost_history,'b.')

def add_ones(val): 
    return np.c_[np.ones(val.shape[0]), val] 
w[0][0] -=  w[1][0]*X.mean()/X.std()
w[1][0] /=  X.std()

print("%.3f" % w[0])
print("%.3f" % w[1])

y_hat = np.dot(add_ones(X), w)  
mse(y, y_hat)

def predict(val):
    val = (val - X.mean()) / X.std()
    return val * w[1][0] + w[0][0]

df.plot.scatter(x="Weight", y="Height")
plt.plot([x_min,x_max], [x_min*w[1]+w[0],x_max*w[1]+w[0]], color="red")
# plt.show()

print("Задача 3. Нахождение параметров модели минимизацией функционала ошибки стохастическим градиентным спуском:")
np.random.seed(42)

def stochastic_grad_descent(X, y, w_init=[70.1, 0.8], eta=1e-4, max_iter=1e4, min_weight_dist=1e-8):
    X = center(X)
    w = np.array(w_init).reshape(-1,1)
    learning_rate = eta
    iterations = int(max_iter)    
    m = len(y)
    cost_history = np.zeros(iterations)
    for it in range(iterations):
        cost = 0.0
        total_diff = np.zeros(w.shape)
        for i in range(m):
            rand_ind = np.random.randint(0,m)
            X_i = X[rand_ind,:].reshape(1,X.shape[1])
            y_i = y[rand_ind].reshape(1,1)
            prediction = np.dot(X_i,w)
            diff = (1/m)*learning_rate*(X_i.T.dot((prediction - y_i)))
            total_diff += diff
            cost += cal_cost(w,X_i,y_i)        
        if (abs((total_diff)) < min_weight_dist).any():
            cost_history = cost_history[:it]
            break        
        w = w - total_diff
        cost_history[it]  = cost       
    return w, cost_history

w,cost_history = stochastic_grad_descent(X, y, w_init=[70.1, 0.8], eta=1e-2, max_iter=1e3, min_weight_dist=1e-8)
fig,ax = plt.subplots(figsize=(12,8))
ax.set_ylabel('J(w)')
ax.set_xlabel('Iterations')
_=ax.plot(range(len(cost_history)),cost_history,'b.')
w[0][0] -=  w[1][0]*X.mean()/X.std()
w[1][0] /=  X.std()

print("%.3f" % w[0])
print("%.3f" % w[1])

y_hat = np.dot(add_ones(X), w)  
mse(y, y_hat)
df.plot.scatter(x="Weight", y="Height")
plt.plot([x_min,x_max], [x_min*w[1]+w[0],x_max*w[1]+w[0]], color="red")
# plt.show()