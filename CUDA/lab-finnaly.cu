#include <stdio.h>
#include <cstdlib>
#include <cmath>

#define SIZE 1000
#define dim 64

/*
Программа с использованием GPU состоит из 2 частей:
– Код на C/C++ (в т.ч. функция main) исполняется как
обычно на CPU.
– На GPU исполняются специальные функции – ядра
(kernel) и функции, вызываемые внутри них.
- Ядро является потоковой функцией – большое количество
потоков (threads) параллельно исполняют тело ядра.
- Ядро вызывается со стороны CPU, при этом указывается
количество потоков, которое будет его исполнять.
- Модель вычислений на GPU во многом напоминает
потоковую модель вычислений.
- Замечание: в русскоязычной литературе GPU-потоки часто
называются нитями. 

Терминология:
– хост (host) = CPU;
– устройство (device) = GPU;
– ядро (kernel) – функция, параллельно выполняемая
потоками на GPU

__host__ (по умолчанию) – функция, вызываемая с хоста и
выполняемая на нем; все обычные функции на С++
попадают в эту категорию.
__global__ – функция, вызываемая с хоста и выполняемая
потоками на устройстве (ядро), всегда возвращает void.
__device__ – функция, вызываемая (одним потоком) и
выполняемая на устройстве.
*/

__global__ void kernel(float *dA, float *dB, float *dC) {
	// В коде на стороне GPU доступны следующие переменные:
	// 	– gridDim – размер решетки блоков;
	// 	– blockIdx – индекс блока потоков внутри решетки;
	// 	– blockDim – размер блока потоков;
	// 	– threadIdx – индекс потока внутри блока потоков
        int i = blockIdx.x * blockDim.x + threadIdx.x;
        if (i < SIZE)
                dC[i] = (pow(sin(dA[i]) * cos(dB[i]), 2) + pow(cos(dA[i]) * sin(dB[i]), 2)) * dA[i] / dB[i];
}

float* GPUGlobalMemoryMethod(float A[], float B[])
{
        float *dA, *dB, *dC;
        float vectorResult[SIZE];

        cudaEvent_t start, stop;
        float timerValueGPUGlobal;

        cudaMalloc(&dA, SIZE * sizeof(int));
        cudaMalloc(&dB, SIZE * sizeof(int));
        cudaMalloc(&dC, SIZE * sizeof(int));

        cudaMemcpy(dA, A, SIZE * sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(dB, B, SIZE * sizeof(int), cudaMemcpyHostToDevice);

        cudaEventCreate(&start);
        cudaEventCreate(&stop);

        // start
        cudaEventRecord(start, 0);
        // Запуск ядра kernel с аргументами (dA, dB, dC) на
		// решетке из '(SIZE + dim - 1) / dim' блоков по 'dim' потоков в каждом
		// всего (SIZE + dim - 1) / dim * dim  потоков.
        kernel <<< (SIZE + dim - 1) / dim, dim >>> (dA, dB, dC);
        cudaEventRecord(stop, 0);
        // 

        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&timerValueGPUGlobal, start, stop);

        cudaEventDestroy(start);
        cudaEventDestroy(stop);

        cudaDeviceSynchronize();
        cudaMemcpy(vectorResult, dC, SIZE * sizeof(int), cudaMemcpyDeviceToHost);

        cudaFree(dA);
        cudaFree(dB);
        cudaFree(dC);

        float* newVector = new float[SIZE];
        for (int i = 0; i < SIZE; i++)
                newVector[i] = vectorResult[i];

        printf("GPU global memory: %f ms\n", timerValueGPUGlobal);

        return newVector;
}

float* CPUMethod(float A[], float B[])
{
        float dC[SIZE];

        cudaEvent_t start, stop;
        float timerValueCPU;

        cudaEventCreate(&start);
        cudaEventCreate(&stop);
        
        // start
        cudaEventRecord(start, 0);
        for (int i = 0; i < SIZE; i++)
        {
                dC[i] = (pow(sin(A[i]) * cos(B[i]), 2) + pow(cos(A[i]) * sin(B[i]), 2)) * A[i] / B[i];
        }
        cudaEventRecord(stop, 0);
        // 

        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&timerValueCPU, start, stop);

        cudaEventDestroy(start);
        cudaEventDestroy(stop);

        float* newVector = new float[SIZE];
        for (int i = 0; i < SIZE; i++)
                newVector[i] = dC[i];

        printf("CPU: %f ms\n", timerValueCPU);

        return newVector;
}

float* GPUPinnedMemoryMethod(float A[], float B[])
{
        float *dA, *dB, *dC;
        float vectorResult[SIZE];

        cudaEvent_t start, stop;
        float timerValueGPUPinned;

        cudaMallocHost(&dA, SIZE * sizeof(int));
        cudaMallocHost(&dB, SIZE * sizeof(int));
        cudaMallocHost(&dC, SIZE * sizeof(int));

        cudaMemcpy(dA, A, SIZE * sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(dB, B, SIZE * sizeof(int), cudaMemcpyHostToDevice);

        cudaEventCreate(&start);
        cudaEventCreate(&stop);
        
        // start
        cudaEventRecord(start, 0);
        kernel <<<(SIZE + dim - 1) / dim, dim >>>(dA, dB, dC);
        cudaEventRecord(stop, 0);
        // 

        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&timerValueGPUPinned, start, stop);

        cudaEventDestroy(start);
        cudaEventDestroy(stop);

        cudaDeviceSynchronize();
        cudaMemcpy(vectorResult, dC, SIZE * sizeof(int), cudaMemcpyDeviceToHost);

        cudaFreeHost(dA);
        cudaFreeHost(dB);
        cudaFreeHost(dC);

        float* newVector = new float[SIZE];
        for (int i = 0; i < SIZE; i++)
 		       newVector[i] = vectorResult[i];

        printf("GPU pinned memory: %f ms\n", timerValueGPUPinned);

        return newVector;
}

float* GPUStreamMethod(float A[], float B[])
{
        float *dA, *dB, *dC;
        float vectorResult[SIZE];

        cudaEvent_t start, stop;
        float timerValueGPUStream;

        int const streamsCount = 2;
        cudaStream_t streams[streamsCount];
        for (int i = 0; i < streamsCount; i++)
                cudaStreamCreate(&streams[i]);

        cudaMallocHost(&dA, SIZE * sizeof(int));
        cudaMallocHost(&dB, SIZE * sizeof(int));
        cudaMallocHost(&dC, SIZE * sizeof(int));

        for (int i = 0; i < streamsCount; i++)
        {
                cudaMemcpyAsync(dB + i*SIZE, B + i*SIZE, SIZE * sizeof(int),
                        cudaMemcpyHostToDevice, streams[i]);
        }

        for (int i = 0; i < streamsCount; i++)
        {
                cudaMemcpyAsync(dA + i*SIZE, A + i*SIZE, SIZE * sizeof(int),
                        cudaMemcpyHostToDevice, streams[i]);
        }

        cudaEventCreate(&start);
        cudaEventCreate(&stop);
	    
	    // start
        cudaEventRecord(start, 0);
        for (int i = 0; i < streamsCount; i++)
                kernel <<< (SIZE + dim - 1) / dim, dim, 0, streams[i] >>>
               (dA + i*SIZE, dB + i*SIZE, dC + i*SIZE);
        cudaEventRecord(stop, 0);
		// 

        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&timerValueGPUStream, start, stop);

        cudaEventDestroy(start);
        cudaEventDestroy(stop);

        for (int i = 0; i < streamsCount; ++i)
                cudaMemcpyAsync(vectorResult + i*SIZE, dC + i*SIZE, SIZE * sizeof(int),
                        cudaMemcpyDeviceToHost, streams[i]);

        cudaDeviceSynchronize();

        for (int i = 0; i < streamsCount; ++i)
                cudaStreamDestroy(streams[i]);

        float* newVector = new float[SIZE];
        for (int i = 0; i < SIZE; i++)
                newVector[i] = vectorResult[i];

        printf("GPU streams: %f ms\n", timerValueGPUStream);

        return newVector;
}

int main() {
        float A[SIZE];
        float B[SIZE];

        for (int i = 0; i < SIZE; i++)
        {
                A[i] = (rand() % 10000) / 100.0;
        }

        for (int i = 0; i < SIZE; i++)
        {
                B[i] = (rand() % 10000) / 100.0;
        }

        float* GPUGlobal = GPUGlobalMemoryMethod(A, B);
        float* CPU = CPUMethod(A, B);
        float* GPUStream = GPUStreamMethod(A, B);
        float* GPUPinned = GPUPinnedMemoryMethod(A, B);

        return 0;
}

// - NVIDIA CUDA C Programming Guide
// - Материалы курса по CUDA и OpenACC:
// http://www.nvidia.ru/object/cuda-openacc-online-courseru.html
// - Боресков А.В. и др. «Параллельные вычисления на GPU.
// Архитектура и программная модель CUDA: Учебное
// пособие»
// - Сандерс Дж., Кэндрот Э. «Технология CUDA в примерах:
// введение в программирование графических процессоров»
// - Farber R. CUDA Application Design and Development
// - http://hpc-education.unn.ru/files/schools/hpc-2014/gpu/Lecture2_IntroToCUDA.pdf
