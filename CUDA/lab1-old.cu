#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

__global__ void function(float *dA, float *dB, float *dC, int size) {
	int i = blockIdx.x * blockDim.x + threadIdx.x;

	if ( i < size )
	{
		 //dC[i] = sinf(sinf(dA[i]*dB[i]));
		 dC[i] = dA[i] + dB[i];
	}
}

int main(int argc, char *argv[]) {
	// Èíèöèàëèçàöèÿ ïåðåìåííûõ-ñîáûòèé äëÿ òàéìåðà
	float timerValueGPU, timerValueCPU;
	cudaEvent_t start, stop;
	float CPUstart, CPUstop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	float *hA, *hB, *hC, *dA, *dB, *dC;
	int size = 512 * 50000;
	int N_thread = 512;
	int N_blocks, i;

	// Çàäàíèå ìàññèâîâ hA, hB, hC äëÿ host
	unsigned int mem_size = sizeof(float)*size;
	hA = (float*)malloc(mem_size);
	hB = (float*)malloc(mem_size);
	hC = (float*)malloc(mem_size);

	// Âûäåëåíèå ïàìÿòè íà device ïîä ìàññèâû dA, dB, dC
	cudaMalloc((void**)&dA, mem_size);
	cudaMalloc((void**)&dB, mem_size);
	cudaMalloc((void**)&dC, mem_size);

	// Çàïîëíåíèå ìàññèâîâ hA, hB è îáíóëåíèå hC
	for (i = 0; i < size; i++) {
		hA[i] = 1.0f / ((i + 1.0f)*(i + 1.0f));
		hB[i] = expf(1.0f/(i+1.0f));
		hC[i] = 0.0f;
	}

	// Îïðåäåíèå ÷èñëà áëîêîâ
	if ((size % N_thread) == 0) {
		N_blocks = size / N_thread;
	}
	else {
		N_blocks = (int)(size/N_thread) + 1;
	}
	dim3 blocks(N_blocks);

	printf("\n N = %d, N_blocks = %d, N_thread = %d\n", size, N_blocks, N_thread);
		 
	// ---------------------GPU---------------------
	// Ñòàðò òàéìåðà
	cudaEventRecord(start, 0);
	// Êîïèðîâàíèå	ìàññèâîâ	ñ host íà device
	cudaMemcpy(dA, hA, mem_size, cudaMemcpyHostToDevice); 
	cudaMemcpy(dB, hB, mem_size, cudaMemcpyHostToDevice);
	// Çàïóñê ôóíêöèè-ÿäðà
	function <<< N_blocks, N_thread >>> (dA, dB, dC, size);
	// Êîïèðîâàíèå	ðåçóëüòàò	ñ device íà host
	cudaMemcpy(hC, dC, mem_size, cudaMemcpyDeviceToHost);
	// Îñòàíîâêà	òàéìåðà è âûâîä	âðåìåíè
	// âû÷èñëåíèÿ GPU âàðèàíòà 
	cudaEventRecord ( stop,	0 ); 
	cudaEventSynchronize ( stop );
	cudaEventElapsedTime(&timerValueGPU, start, stop); 
	printf("\n GPU calculation time: %f ms\n", timerValueGPU);

	// ---------------------CPU---------------------
	CPUstart = clock();
	//for(i=0;i<size;i++) hC[i]=sinf(sinf(hA[i]*hB[i]));
	for (i = 0; i<size; i++) hC[i] = hA[i] + hB[i];
	CPUstop = clock();
	timerValueCPU = 1000.*(CPUstop - CPUstart) / CLOCKS_PER_SEC;
	printf(" CPU calculation time: %f\n", timerValueCPU);
	printf("\n Rate %f x\n", timerValueCPU / timerValueGPU);

	// Îñâîáîæäåíèå ïàìÿòè íà host
	free(hA);
	free(hB);
	free(hC);

	// Îñâîáîæäåíèå ïàìÿòè íà device
	cudaFree(dA);
	cudaFree(dB);
	cudaFree(dC);

	// Óíè÷òîæåíèå ïåðåìåííûõ-ñîáûòèé
	cudaEventDestroy(start); 
	cudaEventDestroy(stop);
		
	return 0;
}