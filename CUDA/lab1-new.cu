#include <stdio.h>
#include <cstdlib>
#include <cmath>

#define SIZE 1000
#define dim 64

__global__ void kernel(float *dA, float *dB, float *dC) {
        int i = blockIdx.x * blockDim.x + threadIdx.x;
        if (i < SIZE)
                dC[i] = (pow(sin(dA[i]) * cos(dB[i]), 2) + pow(cos(dA[i]) * sin(dB[i]), 2)) * dA[i] / dB[i];
}

float* GPUGlobalMemoryMethod(float A[], float B[])
{
        float *dA, *dB, *dC;
        float vectorResult[SIZE];

        cudaEvent_t start, stop;
        float timerValueGPUGlobal;

        cudaMalloc(&dA, SIZE * sizeof(int));
        cudaMalloc(&dB, SIZE * sizeof(int));
        cudaMalloc(&dC, SIZE * sizeof(int));

        cudaMemcpy(dA, A, SIZE * sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(dB, B, SIZE * sizeof(int), cudaMemcpyHostToDevice);

        cudaEventCreate(&start);
        cudaEventCreate(&stop);
        cudaEventRecord(start, 0);
        kernel << <(SIZE + dim - 1) / dim, dim >> >(dA, dB, dC);
        cudaEventRecord(stop, 0);
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&timerValueGPUGlobal, start, stop);

        cudaEventDestroy(start);
        cudaEventDestroy(stop);

       cudaDeviceSynchronize();
        cudaMemcpy(vectorResult, dC, SIZE * sizeof(int), cudaMemcpyDeviceToHost);

        cudaFree(dA);
        cudaFree(dB);
        cudaFree(dC);

        float* newVector = new float[SIZE];
        for (int i = 0; i < SIZE; i++)
                newVector[i] = vectorResult[i];

        printf("GPU global memory time: %f ms\n", timerValueGPUGlobal);

        return newVector;
}

float* CPUMethod(float A[], float B[])
{
        float dC[SIZE];

        cudaEvent_t start, stop;
        float timerValueCPU;

        cudaEventCreate(&start);
        cudaEventCreate(&stop);
        cudaEventRecord(start, 0);
        for (int i = 0; i < SIZE; i++)
        {
                dC[i] = (pow(sin(A[i]) * cos(B[i]), 2) + pow(cos(A[i]) * sin(B[i]), 2))*A[i] / B[i];
        }
        cudaEventRecord(stop, 0);
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&timerValueCPU, start, stop);

        cudaEventDestroy(start);
        cudaEventDestroy(stop);

        float* newVector = new float[SIZE];
        for (int i = 0; i < SIZE; i++)
                newVector[i] = dC[i];

        printf("CPU time: %f ms\n", timerValueCPU);

        return newVector;
}

float* GPUPinnedMemoryMethod(float A[], float B[])
{
        float *dA, *dB, *dC;
        float vectorResult[SIZE];

        cudaEvent_t start, stop;
        float timerValueGPUPinned;

        cudaMallocHost(&dA, SIZE * sizeof(int));
        cudaMallocHost(&dB, SIZE * sizeof(int));
        cudaMallocHost(&dC, SIZE * sizeof(int));

        cudaMemcpy(dA, A, SIZE * sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(dB, B, SIZE * sizeof(int), cudaMemcpyHostToDevice);

        cudaEventCreate(&start);
        cudaEventCreate(&stop);
        cudaEventRecord(start, 0);
        kernel << <(SIZE + dim - 1) / dim, dim >> >(dA, dB, dC);
        cudaEventRecord(stop, 0);
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&timerValueGPUPinned, start, stop);

        cudaEventDestroy(start);
        cudaEventDestroy(stop);

        cudaDeviceSynchronize();
        cudaMemcpy(vectorResult, dC, SIZE * sizeof(int), cudaMemcpyDeviceToHost);

        cudaFreeHost(dA);
        cudaFreeHost(dB);
        cudaFreeHost(dC);

        float* newVector = new float[SIZE];
        for (int i = 0; i < SIZE; i++)
 		       newVector[i] = vectorResult[i];

        printf("GPU pinned memory time: %f ms\n", timerValueGPUPinned);

        return newVector;
}

float* GPUStreamMethod(float A[], float B[])
{
        float *dA, *dB, *dC;
        float vectorResult[SIZE];

        cudaEvent_t start, stop;
        float timerValueGPUStream;

        int const streamsCount = 2;
        cudaStream_t streams[streamsCount];
        for (int i = 0; i < streamsCount; i++)
                cudaStreamCreate(&streams[i]);

       cudaMallocHost(&dA, SIZE * sizeof(int));
        cudaMallocHost(&dB, SIZE * sizeof(int));
        cudaMallocHost(&dC, SIZE * sizeof(int));

        for (int i = 0; i < streamsCount; i++)
        {
                cudaMemcpyAsync(dB + i*SIZE, B + i*SIZE, SIZE * sizeof(int),
                        cudaMemcpyHostToDevice, streams[i]);
        }

        for (int i = 0; i < streamsCount; i++)
        {
                cudaMemcpyAsync(dA + i*SIZE, A + i*SIZE, SIZE * sizeof(int),
                        cudaMemcpyHostToDevice, streams[i]);
        }

        cudaEventCreate(&start);
        cudaEventCreate(&stop);
        cudaEventRecord(start, 0);
        for (int i = 0; i < streamsCount; i++)
                kernel << < (SIZE + dim - 1) / dim, dim, 0, streams[i] >> >
               (dA + i*SIZE, dB + i*SIZE, dC + i*SIZE);
        cudaEventRecord(stop, 0);
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&timerValueGPUStream, start, stop);

        cudaEventDestroy(start);
        cudaEventDestroy(stop);

        for (int i = 0; i < streamsCount; ++i)
                cudaMemcpyAsync(vectorResult + i*SIZE, dC + i*SIZE, SIZE * sizeof(int),
                        cudaMemcpyDeviceToHost, streams[i]);

        cudaDeviceSynchronize();

        for (int i = 0; i < streamsCount; ++i)
                cudaStreamDestroy(streams[i]);

        float* newVector = new float[SIZE];
        for (int i = 0; i < SIZE; i++)
                newVector[i] = vectorResult[i];

        printf("GPU streams time: %f ms\n", timerValueGPUStream);

        return newVector;
}

int main() {

        float A[SIZE];
        float B[SIZE];

        for (int i = 0; i < SIZE; i++)
        {
                A[i] = (rand() % 10000) / 100.0;
        }
        for (int i = 0; i < SIZE; i++)
        {
                B[i] = (rand() % 10000) / 100.0;
        }
       float* GPUGlobal = GPUGlobalMemoryMethod(A, B);

        float* CPU = CPUMethod(A, B);

        float* GPUPinned = GPUPinnedMemoryMethod(A, B);

        float* GPUStream = GPUStreamMethod(A, B);

        for (int i = 0; i < SIZE; i++)
        {
                printf("((sin(A) + cos(B)^2 + (cos(A) + sin(B)^2))*A/B = %.2f = %.2f = %.2f = %.2f\n",
                        GPUStream[i],
                        GPUGlobal[i],
                        GPUPinned[i],
                        CPU[i]);
        }

        return 0;
}

                

