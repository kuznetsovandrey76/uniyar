`lab1-new.out`  
GPU global memory time: 0.050976 ms  
CPU time: 0.251872 ms  
GPU pinned memory time: 0.024480 ms  
GPU streams time: 0.034112 ms  

`lab1-old.out`  
N = 25600000, N_blocks = 50000, N_thread = 512   
GPU calculation time: 137.597275 ms   
CPU calculation time: 119.073997   
Rate 0.865380 x  